package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"gitlab.wikimedia.org/repos/sre/go-qemutest/pkg/qemutest"
)

func TestMain(m *testing.M) {
	os.Exit(qemutest.QemuTestMain(m))
}

type qemuTest struct{}

func TestQemu(t *testing.T) {
	if qemutest.InQemu() {
		qemutest.RunQemuTests(t, qemuTest{})
		return
	}
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}
	testCases := []qemutest.QemuTestCase{{
		KernelImage: filepath.Join(wd, "testdata", "bzImage-6.1.69"),
		TestId:      "6.1-bookworm",
		Verbose:     true,
	}}
	qemutest.RunQemu(t, testCases, []string{"/usr/bin/find", "/bin/ls"})
}

func (qemuTest) TestHelloWorld(t *testing.T) {
	t.Log("Hello world")
}

func (qemuTest) TestExternalBinary(t *testing.T) {
	for _, binary := range []string{"/usr/bin/find", "/bin/ls"} {
		t.Logf("Running %v", binary)
		cmd := exec.Command(binary, "/")
		_, err := cmd.CombinedOutput()
		if err != nil {
			t.Errorf("Unable to run /usr/bin/ls: %v", err)
		}
	}
}
