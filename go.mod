module gitlab.wikimedia.org/repos/sre/go-qemutest

go 1.19

require (
	github.com/u-root/u-root v0.12.0
	golang.org/x/sys v0.16.0
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/pierrec/lz4/v4 v4.1.14 // indirect
)
